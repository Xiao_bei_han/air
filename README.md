# air

### 代码管理与部署
1. gitee仓库: [https://gitee.com/Xiao_bei_han/air](https://gitee.com/Xiao_bei_han/air)
2. 线上访问地址:  [http://49.234.63.42/xiaobei/air/frontend/index.html#/](http://49.234.63.42/xiaobei/air/frontend/index.html#/)

### 工程配置与编码说明

1. 关于环境配置: 支持开发, 测试, 生产三种环境配置;
2. 关于部署: pm2进行node服务管理, nginx配置动静分离和反向代理;
3. 关于commit: message携带type信息(feat, fix 等);
4. 关于前端页面: 选用Vue作为框架;
5. 关于后台接口: 文档中给出的接口不通, 用node进行简易模拟并random响应信息, 代码见 src/service;
6. 关于请求封装: 基于axios简易封装请求公共方法;
7. 关于移动适配: 采用媒体查询进行移动端识别和样式适配;
8. 关于header和footer: 利用padding进行等比例布局;
9. 关于状态管理: 由于项目复杂程度,未引入Vuex进行状态管理;
10. 关于常量管理: 工程范围常量统一在 src/constants 中分类维护;
11. 关于class: 标签中class明明遵循bem规范;
12. 关于表单校验: 基于校验复杂度, 采用策略对象的方式进行配置和校验;
