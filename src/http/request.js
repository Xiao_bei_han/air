import service from './index';

export class Request {
  instance

  static getInstance() {
    if (!this.instance) {
      this.instance = new Request();
    }
    return this.instance;
  }

  async post(options = {}) {
    const response = await service({
      method: 'post',
      ...options,
    });
    return response;
  }

  async delete(options = {}) {
    const { data } = await service({
      method: 'delete',
      ...options,
    });
    return data;
  }

  async put(options = {}) {
    const response = await service({
      method: 'put',
      ...options,
    });
    return response;
  }

  async get(options = {}) {
    const response = await service({
      method: 'get',
      ...options,
    });
    return response;
  }
}
