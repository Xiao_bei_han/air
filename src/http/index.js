import axios from 'axios';
import prefix from '@/constants/prefix';

const DEFAULT_OPTIONS = {
  baseURL: prefix,
  timeout: 3000,
  headers: {
    timestamp: new Date().getTime(),
    'Content-Type': 'application/json;chareset=UTF-8',
  },
};

const instance = axios.create(DEFAULT_OPTIONS);


instance.interceptors.request.use(
  (config) => config,
  (error) => Promise.reject(error),
);

instance.interceptors.response.use(
  (response) => response,
  (thrown) => {
    console.log(`【AIR Log】- ${thrown.toString()}`);
    return Promise.reject(thrown);
  },
);

export default async function (options) {
  const requestOptions = { ...options };

  try {
    const { data, data: { errno, errmsg } } = await instance.request(requestOptions);
    if (errno) {
      throw new Error(errmsg);
    }
    return data;
  } catch (err) {
    throw err;
  }
}
