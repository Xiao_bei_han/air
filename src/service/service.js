const express = require('express');

const app = express();
const port = process.env.PORT || 3000;

app.post('/getInvite', (req, res) => {
  const rand = 10 * Math.random();
  const resultCode = rand > 5 ? '000000' : '999999';
  const resultMsg = rand > 5 ? 'success!' : 'Server Failed...';

  res.send({
    resultCode,
    resultMsg,
  });
});
app.get('/getInvite', (req, res) => {
  const rand = 10 * Math.random();
  const resultCode = rand > 5 ? '000000' : '999999';
  const resultMsg = rand > 5 ? 'success!' : 'Server Failed...';

  res.send({
    resultCode,
    resultMsg,
  });
});

app.listen(port, () => {
  console.log(`listening on port ${ port }`);
});
