import Vue from 'vue';
import { Request } from '@/http/request';
import api from '@/constants/api';
import code from '@/constants/code';
import App from './App.vue';
import router from './router';
import utils from './utils';

Vue.config.productionTip = false;
Vue.prototype.$http = Request.getInstance();
Vue.prototype.$api = api;
Vue.prototype.$code = code;

const VConsole = require('./utils/vconsole.min.js');

if (process.env.NODE_ENV !== 'production') {
  new VConsole(); /* eslint-disable-line*/
}

Vue.prototype.$utils = utils;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
