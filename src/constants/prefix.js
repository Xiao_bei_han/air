// envs
const DEV = 'development';
const PROD = 'production';

// http prefix
let prefix = 'http://49.234.63.42';

if (process.env.NODE_ENV === DEV) {
  prefix = 'http://localhost:3000';
} else if (process.env.NODE_ENV === PROD) {
  prefix = 'http://49.234.63.42/airService';
}

export default prefix;
