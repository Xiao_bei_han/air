const emailSch = /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/;

const validator = {
  name: {
    validateFn: (name) => name.trim().length >= 3,
    errMsg: 'at least 3 characters long',
  },
  email: {
    validateFn: (email) => emailSch.test(email.trim()),
    errMsg: 'please check the email',
  },
  cemail: {
    validateFn: (email, cemail) => emailSch.test(email.trim()) && email.trim() === cemail.trim(),
    errMsg: 'emails should be the same',
  },
};

export default validator;
